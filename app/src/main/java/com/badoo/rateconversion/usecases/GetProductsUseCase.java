package com.badoo.rateconversion.usecases;

import com.badoo.rateconversion.data.DatabaseWrapper;
import com.badoo.rateconversion.executor.BaseUseCase;
import com.badoo.rateconversion.model.busresponses.DataResponse;
import com.badoo.rateconversion.model.busresponses.ProductsResponse;
import com.squareup.otto.Bus;

public class GetProductsUseCase extends BaseUseCase<ProductsResponse> {

    private DatabaseWrapper wrapper;

    public GetProductsUseCase(Bus bus, DatabaseWrapper wrapper) {
        super(bus);
        this.wrapper = wrapper;
    }

    @Override
    public void execute() {
        ProductsResponse productsResponse = wrapper.getProducts();
        handleResponse(productsResponse);
    }
}
