package com.badoo.rateconversion.usecases;

import android.os.Bundle;
import android.text.TextUtils;

import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.data.DatabaseWrapper;
import com.badoo.rateconversion.executor.BaseUseCase;
import com.badoo.rateconversion.model.busresponses.AmountResponse;
import com.badoo.rateconversion.model.busresponses.TransactionResponse;
import com.squareup.otto.Bus;

public class GetTransactionsBySkuUseCase extends BaseUseCase<TransactionResponse> {

    private DatabaseWrapper wrapper;

    public GetTransactionsBySkuUseCase(Bus bus, DatabaseWrapper wrapper) {
        super(bus);
        this.wrapper = wrapper;
    }

    @Override
    public void execute() {
        Bundle params = getParams();
        String sku = params.getString(Constants.KEY_SKU);
        TransactionResponse transactionResponse;
        if (TextUtils.isEmpty(sku)) {
            transactionResponse = new TransactionResponse();
            transactionResponse.setError(Constants.Errors.INVALID_FIELD);
            handleResponse(transactionResponse);
            return;
        }
        transactionResponse = wrapper.getTransactionsForSku(sku);
        handleResponse(transactionResponse);
    }

    @Override
    public void postResponseToBus(TransactionResponse response) {
        super.postResponseToBus(response);
        AmountResponse amountResponse = new AmountResponse();
        amountResponse.setResponse(response.getSum());
        bus.post(amountResponse);
    }
}
