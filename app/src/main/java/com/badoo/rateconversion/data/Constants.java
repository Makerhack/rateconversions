package com.badoo.rateconversion.data;

public class Constants {
    public static final String DATABASE_NAME = "db_projects";
    public static final int DATABASE_VERSION = 10;
    public static final String TRANSACTIONS_FILE = "transactions.json";
    public static final String RATES_FILE = "rates.json";
    public static final String KEY_SKU = "sku";


    public static class UseCases {
        public static final String USE_CASE = "UseCase";
        public static final int GET_PRODUCTS = 0;
        public static final int GET_TRANSACTIONS = 1;
    }

    public class Errors {
        public static final int SERVER_ERROR = 1;
        public static final int EMPTY_RESPONSE = -1;
        public static final int ERROR_CANT_PARSE = -2;
        public static final int ERROR_SERVICE_UNAVAILABLE = 404;
        public static final int INVALID_FIELD = 666;
        public static final int UNPRESENT_RATES = 5;
    }
}
