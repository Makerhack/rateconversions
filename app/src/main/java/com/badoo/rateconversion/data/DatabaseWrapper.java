package com.badoo.rateconversion.data;

import android.text.TextUtils;

import com.badoo.rateconversion.algorithm.TransactionConverter;
import com.badoo.rateconversion.model.Rate;
import com.badoo.rateconversion.model.Transaction;
import com.badoo.rateconversion.model.busresponses.ProductsResponse;
import com.badoo.rateconversion.model.busresponses.TransactionResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.QueryBuilder;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class DatabaseWrapper implements DataKeeper, DataProvider {

    private DataBaseHelper helper;
    private Gson gson;

    public DatabaseWrapper(DataBaseHelper helper) {
        this.helper = helper;
    }


    @Override
    public void storeTransactions(final List<Transaction> transactions) {
        final Dao<Transaction, Integer> transactionsDao = helper.getTransactionsDao();
        try {
            TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (Transaction transaction : transactions) {
                        transactionsDao.create(transaction);
                    }
                    return null;
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean hasTransactionsDataSetLoaded() {
        try {
            return helper.getTransactionsDao().countOf() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public ProductsResponse getProducts() {
        if (hasTransactionsDataSetLoaded()) {
            return orderTransactionsByProduct();
        }
        return createAndOrderDataset();
    }

    private ProductsResponse createAndOrderDataset() {
        String transactionsString = loadJSONFromAsset(Constants.TRANSACTIONS_FILE);
        ProductsResponse productsResponse = new ProductsResponse();
        if (transactionsString == null || transactionsString.isEmpty()) {
            productsResponse.setError(Constants.Errors.EMPTY_RESPONSE);
            return productsResponse;
        }
        Type type = new TypeToken<List<Transaction>>() {

        }.getType();
        List<Transaction> transactions = null;
        try {
            transactions = getGson().fromJson(transactionsString, type);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        if (transactions == null || transactions.isEmpty()){
            productsResponse.setError(Constants.Errors.ERROR_CANT_PARSE);
            return productsResponse;
        }
        storeTransactions(transactions);
        return orderTransactionsByProduct();
    }

    private ProductsResponse orderTransactionsByProduct() {
        ProductsResponse productsResponse = new ProductsResponse();
        Dao<Transaction, Integer> transactionsDao = helper.getTransactionsDao();
        QueryBuilder<Transaction, Integer> builder = transactionsDao.queryBuilder();
        builder.selectRaw("COUNT(*), _id, amount, sku, currency");
        builder.groupBy("sku");
        GenericRawResults<String[]> results;
        List<String[]> stringResults = null;
        try {
            results = builder.queryRaw();
            if (results != null) {
                stringResults = results.getResults();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (stringResults == null || stringResults.isEmpty()) {
            productsResponse.setError(Constants.Errors.EMPTY_RESPONSE);
            return productsResponse;
        }
        List<Transaction> transactions = new ArrayList<>(stringResults.size());
        for (String[] strings : stringResults) {
            transactions.add(new Transaction(strings));
        }
        productsResponse.setResponse(transactions);
        return productsResponse;
    }

    private String loadJSONFromAsset(String fileName) {
        String jsonString;
        try {
            InputStream is = helper.getContext().getAssets().open(fileName);
            BufferedInputStream bin = new BufferedInputStream(new DataInputStream(is));
            byte[] buffer = new byte[bin.available()];
            bin.read(buffer);
            is.close();
            jsonString = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonString;
    }

    @Override
    public boolean hasRatesDataSetLoaded() {
        try {
            return helper.getRatesDao().countOf() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public TransactionResponse getTransactionsForSku(String sku) {
        if (hasRatesDataSetLoaded()) {
            return convertRatesDataset(sku);
        }
        return createAndConvertRatesDataset(sku);

    }

    @Override
    public void storeRates(List<Rate> rates) {
        Dao<Rate, Integer> ratesDao = helper.getRatesDao();
        try {
            for (Rate rate : rates) {
                ratesDao.createOrUpdate(rate);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private TransactionResponse convertRatesDataset(String sku) {
        TransactionResponse response = new TransactionResponse();
        Dao<Rate, Integer> ratesDao = helper.getRatesDao();
        List<Rate> rates = null;
        try {
            rates = ratesDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (rates == null || rates.isEmpty()) {
            response.setError(Constants.Errors.UNPRESENT_RATES);
            return response;
        }
        Dao<Transaction, Integer> transactionsDao = helper.getTransactionsDao();
        List<Transaction> transactions = null;
        try {
            transactions = transactionsDao.queryForEq("sku", sku);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (transactions == null || transactions.isEmpty()) {
            response.setError(Constants.Errors.EMPTY_RESPONSE);
            return response;
        }
        TransactionConverter converter = new TransactionConverter(transactions, rates);
        List<Transaction> convertedTransactions = converter.convert();
        if (convertedTransactions == null || convertedTransactions.isEmpty()) {
            response.setError(Constants.Errors.EMPTY_RESPONSE);
            return response;
        }
        response.setResponse(convertedTransactions);
        response.setSum(converter.getSum());
        return response;
    }


    private TransactionResponse createAndConvertRatesDataset(String sku) {
        TransactionResponse response = new TransactionResponse();
        String conversionRatesJson = loadJSONFromAsset(Constants.RATES_FILE);
        if (TextUtils.isEmpty(conversionRatesJson)) {
            response.setError(Constants.Errors.UNPRESENT_RATES);
            return response;
        }
        Type type = new TypeToken<List<Rate>>() {

        }.getType();
        List<Rate> rates = null;
        try {
            rates = getGson().fromJson(conversionRatesJson, type);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        if (rates == null || rates.isEmpty()) {
            response.setError(Constants.Errors.UNPRESENT_RATES);
            return response;
        }
        storeRates(rates);
        return convertRatesDataset(sku);
    }

    private Gson getGson(){
        if (gson == null) {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Float.class, new FloatTypeAdapter());
            builder.registerTypeAdapter(float.class, new FloatTypeAdapter());
            builder.registerTypeAdapter(Integer.class, new IntegerTypeAdapter());
            builder.registerTypeAdapter(int.class, new IntegerTypeAdapter());
            builder.registerTypeAdapter(Double.class, new DoubleTypeAdapter());
            builder.registerTypeAdapter(double.class, new DoubleTypeAdapter());
            gson = builder.create();
        }
        return gson;
    }
}
