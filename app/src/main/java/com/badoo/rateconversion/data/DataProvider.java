package com.badoo.rateconversion.data;

import com.badoo.rateconversion.model.busresponses.ProductsResponse;
import com.badoo.rateconversion.model.busresponses.TransactionResponse;

interface DataProvider {

    boolean hasTransactionsDataSetLoaded();
    ProductsResponse getProducts();
    boolean hasRatesDataSetLoaded();
    TransactionResponse getTransactionsForSku(String sku);
}
