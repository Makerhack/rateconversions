package com.badoo.rateconversion.data;

import com.badoo.rateconversion.model.Rate;
import com.badoo.rateconversion.model.Transaction;

import java.util.List;

interface DataKeeper {

    void storeTransactions(List<Transaction> transactions);
    void storeRates(List<Rate> rates);

}
