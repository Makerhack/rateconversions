package com.badoo.rateconversion.model.busresponses;

public class DataResponse<T> {

    protected T response;
    protected int error;

    public DataResponse() {
        this.error = 0;
    }


    public DataResponse(DataResponse<T> other) {
        this.response = other.getResponse();
        this.error = other.error;
    }

    public DataResponse(T object) {
        this.response = object;
        this.error = 0;
    }

    public DataResponse(int error) {
        this.response = null;
        this.error = error;
    }


    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public boolean hasError() {
        return error != 0;
    }
}
