package com.badoo.rateconversion.model.busresponses;

import com.badoo.rateconversion.model.Transaction;

import java.util.List;

public class TransactionResponse extends DataResponse<List<Transaction>> {
    private float sum;

    public void setSum(float sum) {
        this.sum = sum;
    }

    public float getSum() {
        return sum;
    }
}
