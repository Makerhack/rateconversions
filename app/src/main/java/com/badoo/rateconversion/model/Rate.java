package com.badoo.rateconversion.model;


import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

public class Rate {
    @DatabaseField(generatedId = true)
    protected int _id;
    @SerializedName("from")
    @DatabaseField
    protected String from;
    @SerializedName("to")
    @DatabaseField
    protected String to;
    @SerializedName("rate")
    @DatabaseField
    protected float rate;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rate rate1 = (Rate) o;

        return Float.compare(rate1.rate, rate) == 0 && from.equals(rate1.from) && to.equals(rate1.to);

    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + (rate != +0.0f ? Float.floatToIntBits(rate) : 0);
        return result;
    }
}
