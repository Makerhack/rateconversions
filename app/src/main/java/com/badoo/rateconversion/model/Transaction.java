package com.badoo.rateconversion.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;


public class Transaction {

    @DatabaseField(generatedId = true)
    protected int _id;
    @SerializedName("amount")
    @DatabaseField
    protected float amount;
    @SerializedName("sku")
    @DatabaseField
    protected String sku;
    @SerializedName("currency")
    @DatabaseField
    protected String currency;

    protected float calculatedAmmount;
    protected int count;


    public Transaction() {

    }

    public Transaction(String[] strings) {
        if (strings.length > 4) {
            try {
                count = Integer.parseInt(strings[0]);
                _id = Integer.parseInt(strings[1]);
                amount = Float.parseFloat(strings[2]);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
            sku = strings[3];
            currency = strings[4];
        }
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getCalculatedAmmount() {
        return calculatedAmmount;
    }

    public void setCalculatedAmmount(float calculatedAmmount) {
        this.calculatedAmmount = calculatedAmmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        return Float.compare(that.amount, amount) == 0 && sku.equals(that.sku) && currency.equals(that.currency);

    }

    @Override
    public int hashCode() {
        int result = (amount != +0.0f ? Float.floatToIntBits(amount) : 0);
        result = 31 * result + sku.hashCode();
        result = 31 * result + currency.hashCode();
        return result;
    }
}
