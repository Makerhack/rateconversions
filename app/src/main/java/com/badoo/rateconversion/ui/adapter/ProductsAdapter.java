package com.badoo.rateconversion.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.badoo.rateconversion.R;
import com.badoo.rateconversion.common.recyclerviewutils.ItemRecyclerAdapter;
import com.badoo.rateconversion.common.recyclerviewutils.ItemViewHolder;
import com.badoo.rateconversion.model.Transaction;

import java.text.NumberFormat;
import java.util.Currency;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ProductsAdapter extends ItemRecyclerAdapter<Transaction, ProductsAdapter.ProductsHolder> {


    public ProductsAdapter(Context context) {
        super(context);
    }

    @Override
    public ProductsHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = getLayoutInflater().inflate(R.layout.row_product, viewGroup, false);
        return new ProductsHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductsHolder holder, int position, Transaction item) {
        holder.skuTv.setText(item.getSku());
        int transactionCount = item.getCount();
        String transactions = context.getResources().getQuantityString(R.plurals.transactions,
                transactionCount, transactionCount);
        holder.sumTv.setText(transactions);
    }

    static class ProductsHolder extends ItemViewHolder<Transaction> {
        @Bind(R.id.sku_tv)
        TextView skuTv;
        @Bind(R.id.sum_tv)
        TextView sumTv;

        ProductsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
