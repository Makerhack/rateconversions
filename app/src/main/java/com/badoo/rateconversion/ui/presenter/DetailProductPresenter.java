package com.badoo.rateconversion.ui.presenter;

import android.content.Context;
import android.os.Bundle;

import com.badoo.rateconversion.basecomponents.BasePresenter;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.model.busresponses.AmountResponse;
import com.badoo.rateconversion.model.busresponses.ProductsResponse;
import com.badoo.rateconversion.model.busresponses.TransactionResponse;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class DetailProductPresenter extends BasePresenter<TransactionsView> {

    public DetailProductPresenter(Bus bus, Context context) {
        super(bus, context);
    }

    @Override
    protected int getUseCaseNumber() {
        return Constants.UseCases.GET_TRANSACTIONS;
    }

    public void getTransactions(String sku) {
        Bundle bundle = getUseCaseBundle();
        bundle.putString(Constants.KEY_SKU, sku);
        startUseCaseService(bundle);
    }

    @Subscribe
    public void onReceiveProducts(TransactionResponse response){
        super.onReceive();
        viewInterface.onTransactions(response.getResponse());
    }

    @Subscribe
    public void onReceiveAmount(AmountResponse response){
        super.onReceive();
        viewInterface.onTransactionSum(response.getResponse());
    }

    @Subscribe
    public void onError(Integer error){
        super.onError(error);
    }
}
