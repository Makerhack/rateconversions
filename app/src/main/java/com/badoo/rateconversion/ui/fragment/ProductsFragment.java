package com.badoo.rateconversion.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.badoo.rateconversion.R;
import com.badoo.rateconversion.basecomponents.fragment.InjectableFragment;
import com.badoo.rateconversion.common.recyclerviewutils.DividerItemDecorator;
import com.badoo.rateconversion.common.recyclerviewutils.ItemRecyclerAdapter;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.model.Transaction;
import com.badoo.rateconversion.ui.activity.ProductDetailActivity;
import com.badoo.rateconversion.ui.adapter.ProductsAdapter;
import com.badoo.rateconversion.ui.presenter.ProductsPresenter;
import com.badoo.rateconversion.ui.presenter.ProductsView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ProductsFragment extends InjectableFragment implements ProductsView {

    public static final String TAG = ProductsFragment.class.getSimpleName();
    public static final int SPACE = 20;

    public static ProductsFragment newInstance(Bundle args) {
        ProductsFragment fragment = new ProductsFragment();
        if (args != null) {
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Inject
    protected ProductsPresenter presenter;

    @Bind(R.id.transactions_rv)
    protected RecyclerView transactionsRv;
    @Bind(R.id.empty_tv)
    protected TextView emptyTv;

    private ProductsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_products, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        transactionsRv.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        transactionsRv.addItemDecoration(new DividerItemDecorator(getContext(),
                DividerItemDecorator.VERTICAL_LIST));
        adapter = new ProductsAdapter(getContext());
        adapter.setOnItemClickListener(new ItemRecyclerAdapter.OnItemClickListener<Transaction>() {
            @Override
            public void onItemClick(Transaction item, View rowView, int position) {
                Intent intent = new Intent(getContext(), ProductDetailActivity.class);
                intent.putExtra(Constants.KEY_SKU, item.getSku());
                startActivity(intent);
            }
        });
        transactionsRv.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onResume(this);
        }
        if (adapter != null && adapter.getItemCount() <= 0) {
            refresh();
        }
    }

    @Override
    public void onPause() {
        if (presenter != null) {
            presenter.onPause();
        }
        super.onPause();
    }

    @Override
    protected void refresh() {
        if (!isWorking()) {
            presenter.getProducts();
        }

    }

    @Override
    public void onTransactions(List<Transaction> transactionList) {
        if (!isAdded()) {
            return;
        }
        if (adapter != null) {
            emptyTv.setVisibility(transactionList.isEmpty() ? View.VISIBLE : View.GONE);
            adapter.set(transactionList);
        }
    }
}
