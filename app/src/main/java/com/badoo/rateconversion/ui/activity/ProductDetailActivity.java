package com.badoo.rateconversion.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.badoo.rateconversion.basecomponents.activity.InjectableActivity;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.ui.fragment.ProductsDetailFragment;

import java.util.List;

public class ProductDetailActivity extends InjectableActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            String sku = getIntent().getStringExtra(Constants.KEY_SKU);
            ft.replace(android.R.id.content, ProductsDetailFragment.newInstance(sku),
                    ProductsDetailFragment.TAG);
            ft.commit();
        }
    }

    @Override
    protected List<Object> getModules() {
        modules.add(new ProductActivityModule());
        return modules;
    }
}
