package com.badoo.rateconversion.ui.activity;

import android.content.Context;

import com.badoo.rateconversion.basecomponents.activity.ActivityContext;
import com.badoo.rateconversion.ui.fragment.ProductsDetailFragment;
import com.badoo.rateconversion.ui.fragment.ProductsFragment;
import com.badoo.rateconversion.ui.presenter.DetailProductPresenter;
import com.badoo.rateconversion.ui.presenter.ProductsPresenter;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(library = true, complete = false,
        injects = {ProductDetailActivity.class, ProductsDetailFragment.class})

public class ProductActivityModule {

    @Provides
    @Singleton
    public DetailProductPresenter provideDetailProductPresenterr(Bus bus, @ActivityContext Context context) {
        return new DetailProductPresenter(bus, context);
    }
}
