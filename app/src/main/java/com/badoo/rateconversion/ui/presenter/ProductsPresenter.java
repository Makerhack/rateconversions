package com.badoo.rateconversion.ui.presenter;

import android.content.Context;
import android.os.Bundle;

import com.badoo.rateconversion.basecomponents.BasePresenter;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.model.busresponses.ProductsResponse;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class ProductsPresenter extends BasePresenter<ProductsView> {

    public ProductsPresenter(Bus bus, Context context) {
        super(bus, context);
    }

    @Override
    protected int getUseCaseNumber() {
        return Constants.UseCases.GET_PRODUCTS;
    }

    public void getProducts() {
        Bundle bundle = getUseCaseBundle();
        startUseCaseService(bundle);
    }

    @Subscribe
    public void onReceiveProducts(ProductsResponse response){
        super.onReceive();
        viewInterface.onTransactions(response.getResponse());
    }

    @Subscribe
    public void onError(Integer error){
        super.onError(error);
    }
}
