package com.badoo.rateconversion.ui.presenter;

import com.badoo.rateconversion.common.WorkerViewInterface;
import com.badoo.rateconversion.model.Transaction;

import java.util.List;


public interface TransactionsView extends WorkerViewInterface {

    void onTransactions(List<Transaction> transactionList);
    void onTransactionSum(float sum);

}
