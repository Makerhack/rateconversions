package com.badoo.rateconversion.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.badoo.rateconversion.R;
import com.badoo.rateconversion.common.recyclerviewutils.ItemRecyclerAdapter;
import com.badoo.rateconversion.common.recyclerviewutils.ItemViewHolder;
import com.badoo.rateconversion.model.Transaction;

import java.util.Currency;

import butterknife.Bind;
import butterknife.ButterKnife;


public class TransactionsAdapter extends ItemRecyclerAdapter<Transaction, TransactionsAdapter.TransactionHolder> {

    public TransactionsAdapter(Context context) {
        super(context);
    }

    @Override
    public TransactionHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = getLayoutInflater().inflate(R.layout.row_transaction, viewGroup, false);
        return new TransactionHolder(v);
    }

    @Override
    public void onBindViewHolder(TransactionHolder holder, int position, Transaction item) {
        holder.originalAmountTv.setText(context.getString(R.string.currency,
                getCurrencyFormatFor(item.getCurrency()), item.getAmount()));
        if (item.getCalculatedAmmount() != -1f) {
            holder.calculatedAmountTv.setText(context.getString(R.string.currency,
                    getCurrencyFormatFor("GBP"), item.getCalculatedAmmount()));
        }else{
            holder.calculatedAmountTv.setText(R.string.not_enought_data);
        }
    }

    private String getCurrencyFormatFor(String currencyCode) {
        Currency currency = Currency.getInstance(currencyCode);
        return currency.getSymbol();
    }

    static class TransactionHolder extends ItemViewHolder<Transaction> {
        @Bind(R.id.original_amount_tv)
        TextView originalAmountTv;
        @Bind(R.id.calculated_amount_tv)
        TextView calculatedAmountTv;

        TransactionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
