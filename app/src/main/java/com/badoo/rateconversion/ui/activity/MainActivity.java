package com.badoo.rateconversion.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.badoo.rateconversion.basecomponents.activity.InjectableActivity;
import com.badoo.rateconversion.ui.fragment.ProductsFragment;

import java.util.List;

public class MainActivity extends InjectableActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(android.R.id.content, ProductsFragment.newInstance(null),
                    ProductsFragment.TAG);
            ft.commit();
        }
    }

    @Override
    protected List<Object> getModules() {
        modules.add(new MainActivityModule());
        return modules;
    }

    //Decorator pattern, let's define how this activity'a ActionBar should look like

    @Override
    protected boolean shouldSetActionBarBack() {
        return false;
    }

    @Override
    protected boolean shouldShowHomeIcon() {
        return true;
    }
}
