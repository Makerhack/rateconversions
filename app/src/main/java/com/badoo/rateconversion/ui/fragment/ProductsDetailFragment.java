package com.badoo.rateconversion.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.badoo.rateconversion.R;
import com.badoo.rateconversion.basecomponents.fragment.InjectableFragment;
import com.badoo.rateconversion.common.recyclerviewutils.DividerItemDecorator;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.model.Transaction;
import com.badoo.rateconversion.ui.adapter.TransactionsAdapter;
import com.badoo.rateconversion.ui.presenter.DetailProductPresenter;
import com.badoo.rateconversion.ui.presenter.TransactionsView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProductsDetailFragment extends InjectableFragment implements TransactionsView {

    public static final String TAG = ProductsDetailFragment.class.getSimpleName();

    public static ProductsDetailFragment newInstance(String sku) {
        Bundle args = new Bundle();
        args.putString("sku", sku);
        ProductsDetailFragment fragment = new ProductsDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    protected DetailProductPresenter presenter;
    @Bind(R.id.transactions_rv)
    protected RecyclerView transactionsRv;
    @Bind(R.id.total_ammount_tv)
    protected TextView ammountTv;
    @Bind(R.id.empty_tv)
    protected TextView emptyTv;

    private TransactionsAdapter adapter;
    private String sku = "", title = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            sku = arguments.getString(Constants.KEY_SKU);

        }
        if (TextUtils.isEmpty(sku) && savedInstanceState != null
                && savedInstanceState.containsKey(Constants.KEY_SKU)) {
            sku = savedInstanceState.getString(Constants.KEY_SKU);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_detail_products, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        transactionsRv.setLayoutManager(new LinearLayoutManager(getContext()));
        transactionsRv.addItemDecoration(new DividerItemDecorator(getContext(),
                DividerItemDecorator.VERTICAL_LIST));
        adapter = new TransactionsAdapter(getContext());
        transactionsRv.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            String title = getString(R.string.transactions_for) + " " + sku;
            getActivity().setTitle(title);
        }
        if (presenter != null) {
            presenter.onResume(this);
        }
        if (adapter != null && adapter.getItemCount() <= 0) {
            refresh();
        }
    }

    @Override
    public void onPause() {
        if (presenter != null) {
            presenter.onPause();
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.KEY_SKU, sku);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void refresh() {
        if (!isWorking()) {
            presenter.getTransactions(sku);
        }

    }

    @Override
    public void onTransactions(List<Transaction> transactionList) {
        if (!isAdded()) {
            return;
        }
        adapter.set(transactionList);
        emptyTv.setVisibility(transactionList.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onTransactionSum(float sum) {
        if (!isAdded()) {
            return;
        }
        ammountTv.setText(getString(R.string.total, sum));
    }
}
