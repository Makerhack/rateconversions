package com.badoo.rateconversion.algorithm;

import java.util.List;

/**
 * Class representing a graph
 * Based on Vogella's
 * http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra
 */

class Graph {
    private final List<Vertex> vertexes;
    private final List<Edge> edges;

    Graph(List<Vertex> vertexes, List<Edge> edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

    List<Vertex> getVertexes() {
        return vertexes;
    }

    List<Edge> getEdges() {
        return edges;
    }
}