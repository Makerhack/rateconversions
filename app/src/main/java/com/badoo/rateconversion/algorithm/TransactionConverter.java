package com.badoo.rateconversion.algorithm;

import com.badoo.rateconversion.model.Rate;
import com.badoo.rateconversion.model.Transaction;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Converts a given set of transactions and rates to a converted transactions list
 */
public class TransactionConverter {
    private List<Transaction> transactions;
    private List<Rate> rates;
    private float sum;

    public TransactionConverter(List<Transaction> transactions, List<Rate> rates) {
        this.transactions = transactions;
        this.rates = rates;
    }

    public List<Transaction> convert() {
        List<Vertex> vertexes = createVertexes(rates);
        List<Edge> edges = createEdges(vertexes, rates);
        Graph graph = new Graph(vertexes, edges);
        DijkstraAlgorithm algorithm = new DijkstraAlgorithm(graph);
        List<Transaction> converted = new ArrayList<>(transactions.size());
        Vertex gbp = findVertex(vertexes, "GBP");
        LinkedList<Vertex> path;
        float convertedAmount;
        for (Transaction transaction : transactions) {
            if (!transaction.getCurrency().equalsIgnoreCase("gbp")) {
                algorithm.execute(findVertex(vertexes, transaction.getCurrency()));
                path = algorithm.getPath(gbp);
                convertedAmount = transaction.getAmount();
                if (path!=null) {
                    for (int i = 0; i < path.size() - 1; i++) {
                        float edgeWeight = findEdgeWeight(path, edges, i);
                        convertedAmount *= edgeWeight;
                    }
                }else{
                    convertedAmount = -1f;
                }
            } else {
                convertedAmount = transaction.getAmount();
            }
            transaction.setCalculatedAmmount(convertedAmount);
            sum += convertedAmount;
            converted.add(transaction);
        }
        return converted;
    }

    private float findEdgeWeight(List<Vertex> vertexes, List<Edge> edges, int i) {
        final Vertex src = vertexes.get(i);
        final Vertex dst = vertexes.get(i + 1);
        List<Edge> filter = new ArrayList<>(Collections2.filter(edges, new Predicate<Edge>() {
            @Override
            public boolean apply(Edge input) {
                return input.getSource().equals(src) && input.getDestination().equals(dst);
            }
        }));
        if (!filter.isEmpty()) {
            return filter.get(0).getWeight();
        }
        return 1f;
    }


    private List<Vertex> createVertexes(List<Rate> rates) {
        ArrayList<Vertex> vertexes = new ArrayList<>();
        ArrayList<String> strings = new ArrayList<>();
        for (Rate rate : rates) {
            checkAndAdd(strings, rate.getFrom());
            checkAndAdd(strings, rate.getTo());
        }
        for (int i = 0; i < strings.size(); i++) {
            vertexes.add(new Vertex(String.valueOf(i), strings.get(i)));
        }
        return vertexes;
    }

    private Vertex findVertex(List<Vertex> vertexes, final String currency) {
        List<Vertex> filter = new ArrayList<>(Collections2.filter(vertexes, new Predicate<Vertex>() {
            @Override
            public boolean apply(Vertex input) {
                return input.getName().equalsIgnoreCase(currency);
            }
        }));
        if (!filter.isEmpty()) {
            return filter.get(0);
        }
        return null;
    }

    private List<Edge> createEdges(List<Vertex> vertexes, List<Rate> rates) {
        ArrayList<Edge> edges = new ArrayList<>();
        for (int i = 0; i < rates.size(); i++) {
            Rate rate = rates.get(i);
            Vertex source = findVertex(vertexes, rate.getFrom());
            Vertex dest = findVertex(vertexes, rate.getTo());
            if (source == null || dest == null) {
                return null;
            }
            edges.add(new Edge(String.valueOf(i), source, dest, rate.getRate()));
        }
        return edges;
    }

    private void checkAndAdd(ArrayList<String> strings, String from) {
        if (!strings.contains(from)) {
            strings.add(from);
        }
    }


    public float getSum() {
        return sum;
    }
}
