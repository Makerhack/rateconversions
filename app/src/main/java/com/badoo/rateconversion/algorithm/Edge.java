package com.badoo.rateconversion.algorithm;

/**
 * Class representing the edged of the graph
 * Based on Vogella's
 * http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra
 */

class Edge  {
    private final String id;
    private final Vertex source;
    private final Vertex destination;
    private final float weight;

    Edge(String id, Vertex source, Vertex destination, float weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }
    Vertex getDestination() {
        return destination;
    }

    public Vertex getSource() {
        return source;
    }
    float getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " " + destination;
    }


}