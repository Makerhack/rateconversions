package com.badoo.rateconversion.basecomponents;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.badoo.rateconversion.common.WorkerViewInterface;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.executor.ExecutorIntentService;
import com.squareup.otto.Bus;


/**
 * Basic class for View controllers used in distinct Fragments
 * Created by Santi on 18/05/2014.
 */
public abstract class BasePresenter<T extends WorkerViewInterface> {
    public static final String ACTION = "action";
    public static final String FORCE_UPDATE = "forceUpdate";
    protected int workerIndex = 0;
    protected Bus bus;
    protected Context context;
    protected T viewInterface;
    private boolean isRegistered = false;

    public BasePresenter(Bus bus, Context context) {
        this.bus = bus;
        this.context = context;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void onResume(T ui) {
        this.viewInterface = ui;
        if (!isRegistered) {
            bus.register(this);
            isRegistered = true;
        }
    }

    public void onPause() {
        if (viewInterface != null && viewInterface.isWorking())
            viewInterface.unsetWorking(workerIndex);
        this.viewInterface = null;
        if (isRegistered) {
            bus.unregister(this);
            isRegistered = false;

        }
    }

    protected Bundle getUseCaseBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.UseCases.USE_CASE, getUseCaseNumber());
        return bundle;
    }

    protected abstract int getUseCaseNumber();

    protected void startUseCaseService(Bundle bundle, boolean forceUpdate) {
        final Intent intent = new Intent(context, ExecutorIntentService.class);
        bundle.putBoolean(FORCE_UPDATE, forceUpdate);
        intent.putExtras(bundle);
        if (viewInterface != null) {
            workerIndex = viewInterface.setWorking();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                context.startService(intent);
            }
        },100);
    }

    protected void startUseCaseService(Bundle bundle) {
        startUseCaseService(bundle, true);
    }

    protected void onSuccess() {
        viewInterface.unsetWorking(workerIndex);
    }

    protected void onReceive() {
        if (viewInterface != null) {
            viewInterface.unsetWorking(workerIndex);
        }
    }

    protected void onError(int error) {
        viewInterface.unsetWorking(workerIndex);
        switch (error) {
            case Constants.Errors.EMPTY_RESPONSE:
                viewInterface.showEmptyError();
                break;
            case Constants.Errors.ERROR_CANT_PARSE:
                viewInterface.showParseError();
                break;
            case Constants.Errors.SERVER_ERROR:
            case Constants.Errors.ERROR_SERVICE_UNAVAILABLE:
                viewInterface.showConnectionError();
                break;
            case Constants.Errors.UNPRESENT_RATES:
                viewInterface.showMissingRatesFileError();
            default:
                break;
        }
    }
}
