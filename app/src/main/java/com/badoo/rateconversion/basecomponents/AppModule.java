package com.badoo.rateconversion.basecomponents;

import android.app.Application;

import com.badoo.rateconversion.common.MainThreadBus;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.data.DataBaseHelper;
import com.badoo.rateconversion.data.DatabaseWrapper;
import com.badoo.rateconversion.executor.ExecutorIntentService;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(library = true,
        injects = {
                BaseApplication.class, ExecutorIntentService.class
        })
class AppModule {
    private final Application application;

    AppModule(Application application) {
        this.application = application;
    }

    @Provides
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public Bus provideBus() {
        return new MainThreadBus();
    }

    @Provides
    @Singleton
    public DataBaseHelper provideDatabaseHelper(Application context) {
        return new DataBaseHelper(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Provides
    @Singleton
    public DatabaseWrapper provideDatabaseWrapper(Application context, DataBaseHelper helper) {
        return new DatabaseWrapper(helper);
    }

}
