package com.badoo.rateconversion.basecomponents;

import android.app.Application;

import com.badoo.rateconversion.R;

import java.util.List;

import dagger.ObjectGraph;

public class BaseApplication extends Application {

    private static BaseApplication sInstance;
    private ObjectGraph objectGraph;

    public static BaseApplication getApp() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        initializeDependencyInjector();
    }


    private void initializeDependencyInjector() {
        objectGraph = ObjectGraph.create(new AppModule(this));
        objectGraph.inject(this);
        objectGraph.injectStatics();
    }

    public ObjectGraph plus(List<Object> modules) {
        if (modules == null) {
            throw new IllegalArgumentException(getString(R.string.invalid_module));
        }
        return objectGraph.plus(modules.toArray());
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }
}
