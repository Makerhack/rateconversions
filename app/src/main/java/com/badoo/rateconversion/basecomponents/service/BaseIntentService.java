package com.badoo.rateconversion.basecomponents.service;

import android.app.IntentService;

import com.badoo.rateconversion.basecomponents.BaseApplication;
import com.badoo.rateconversion.data.DatabaseWrapper;

import javax.inject.Inject;


public abstract class BaseIntentService extends IntentService {
    @Inject
    protected DatabaseWrapper databaseWrapper;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BaseIntentService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        injectDependencies();
    }

    private void injectDependencies() {
        ((BaseApplication) getApplicationContext()).inject(this);
    }

}
