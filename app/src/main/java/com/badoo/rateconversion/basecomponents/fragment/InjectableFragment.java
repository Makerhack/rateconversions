package com.badoo.rateconversion.basecomponents.fragment;

import android.app.Activity;
import android.content.Context;

import com.badoo.rateconversion.basecomponents.activity.InjectableActivity;


public abstract class InjectableFragment extends BaseFragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        injectDependencies();
    }

    private void injectDependencies() {
        ((InjectableActivity) getActivity()).inject(this);
    }

}
