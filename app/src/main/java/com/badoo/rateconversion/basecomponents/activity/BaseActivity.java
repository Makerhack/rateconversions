package com.badoo.rateconversion.basecomponents.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.badoo.rateconversion.R;


public abstract class BaseActivity extends AppCompatActivity {

    protected boolean homeButtonEnabled = true;


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null ) {
            supportActionBar.setDisplayShowHomeEnabled(true);
            if (shouldSetActionBarBack()) {
                supportActionBar.setDisplayHomeAsUpEnabled(homeButtonEnabled);
                supportActionBar.setHomeButtonEnabled(homeButtonEnabled);
            }
            if (shouldShowHomeIcon()){
                supportActionBar.setDisplayUseLogoEnabled(true);
                supportActionBar.setLogo(R.drawable.ic_home_black_24dp);
            }
        }
    }

    protected boolean shouldSetActionBarBack() {
        return true;
    }

    protected boolean shouldShowHomeIcon() {
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    public void showMessage(int resourceId) {
        Snackbar.make(getWindow().getDecorView(), resourceId, Snackbar.LENGTH_LONG).show();
    }

    public void showMessage(String message) {
        Snackbar.make(getWindow().getDecorView(), message, Snackbar.LENGTH_LONG).show();

    }
}
