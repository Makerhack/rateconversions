package com.badoo.rateconversion.basecomponents.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.badoo.rateconversion.R;
import com.badoo.rateconversion.common.WorkerViewInterface;

import java.util.LinkedList;
import java.util.List;


public abstract class BaseFragment extends Fragment implements WorkerViewInterface, SwipeRefreshLayout.OnRefreshListener {

    protected SwipeRefreshLayout swipeRefreshLayout;
    private List<Integer> works = new LinkedList<>();
    private int baseIndex = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(getClass().getSimpleName(), "Fragment Attached");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.d(getClass().getSimpleName(), "Fragment Created");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(getClass().getSimpleName(), "Fragment View Created");
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_zone);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, android.R.color.black);
            swipeRefreshLayout.setEnabled(true);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void showError(String message) {
        showMessage(message);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(getClass().getSimpleName(), "Fragment Resumed");
    }

    @Override
    public void onPause() {
        Log.d(getClass().getSimpleName(), "Fragment Paused");
        super.onPause();
    }

    @Override
    public void onDetach() {
        Log.d(getClass().getSimpleName(), "Fragment Dettached from Activity");
        super.onDetach();
    }


    @Override
    public void onRefresh() {
        if (!isWorking()) {
            refresh();
        }
    }

    protected abstract void refresh();

    @Override
    public void unsetAllWorkings() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }
        works.clear();
    }

    @Override
    public int setWorking() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }
        if (works == null) {
            works = new LinkedList<>();
        }
        works.add(++baseIndex);
        return baseIndex;
    }

    @Override
    public void unsetWorking(int index) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }
        if (works != null) {
            works.remove(Integer.valueOf(index));
        } else {
            works = new LinkedList<>();
        }
    }

    @Override
    public boolean isWorking() {
        return works != null && !works.isEmpty();
    }

    @Override
    public void showConnectionError() {
        if (getActivity() != null) {
            showMessage(R.string.cant_connect);
        }
    }

    @Override
    public void showParseError() {
        if (getActivity() != null) {
            showMessage(R.string.cant_parse);
        }
    }

    @Override
    public void showEmptyError() {
        if (getActivity() != null) {
            showMessage(R.string.no_object);
        }
    }

    @Override
    public void showMessage(int resourceId) {
        if (isAdded() && getView() != null) {
            Snackbar snackbar = Snackbar.make(getView(), resourceId, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    @Override
    public void showMessage(String message) {
        if (isAdded() && getView() != null) {
            Snackbar snackbar = Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    @Override
    public void showMissingRatesFileError() {
        if (getActivity() != null) {
            showMessage(R.string.no_rates);
        }
    }

}
