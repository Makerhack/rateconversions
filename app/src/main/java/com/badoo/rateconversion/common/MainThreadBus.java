package com.badoo.rateconversion.common;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Bus wich uses main thread
 * Created by Santi on 02/01/15.
 */
public class MainThreadBus extends Bus {

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public MainThreadBus() {
        super(ThreadEnforcer.ANY);
    }

    @Override
    public void post(final Object event) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                MainThreadBus.super.post(event);
            }
        });
    }

}
