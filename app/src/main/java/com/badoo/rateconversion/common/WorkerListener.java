package com.badoo.rateconversion.common;

public interface WorkerListener {

    public void onWorking();

    public void onWorkFinished();

}
