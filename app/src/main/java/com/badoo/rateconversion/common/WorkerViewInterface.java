package com.badoo.rateconversion.common;


import android.support.annotation.StringRes;

public interface WorkerViewInterface {

    int setWorking();

    void unsetWorking(int index);

    boolean isWorking();

    void unsetAllWorkings();

    void showConnectionError();

    void showParseError();

    void showEmptyError();

    void showError(String message);

    void showMessage(@StringRes int resourceId);

    void showMessage(String message);

    void showMissingRatesFileError();
}
