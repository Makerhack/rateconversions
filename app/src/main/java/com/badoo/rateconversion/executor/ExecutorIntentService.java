package com.badoo.rateconversion.executor;

import android.content.Intent;
import android.os.Bundle;

import com.badoo.rateconversion.basecomponents.service.BaseIntentService;
import com.badoo.rateconversion.data.Constants;
import com.badoo.rateconversion.usecases.GetProductsUseCase;
import com.badoo.rateconversion.usecases.GetTransactionsBySkuUseCase;
import com.squareup.otto.Bus;

import javax.inject.Inject;


public class ExecutorIntentService extends BaseIntentService implements Executor {

    @Inject
    protected Bus bus;

    public ExecutorIntentService() {
        super("ExecutorIntentService");
    }

    @Override
    public void execute(UseCase useCase, Bundle params) {
        useCase.setParams(params);
        useCase.execute();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Bundle params = intent.getExtras();
            int useCaseNumber = intent.getIntExtra(Constants.UseCases.USE_CASE, 0);
            UseCase useCase = getUseCase(useCaseNumber);
            execute(useCase, params);
        }
    }

    public UseCase getUseCase(int caseNumber) {
        switch (caseNumber) {
            case Constants.UseCases.GET_PRODUCTS:
                return new GetProductsUseCase(bus, databaseWrapper);
            case Constants.UseCases.GET_TRANSACTIONS:
                return new GetTransactionsBySkuUseCase(bus, databaseWrapper);
        }
        return null;
    }

}
