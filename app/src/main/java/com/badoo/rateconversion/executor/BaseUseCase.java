package com.badoo.rateconversion.executor;

import android.os.Bundle;

import com.badoo.rateconversion.basecomponents.BasePresenter;
import com.badoo.rateconversion.model.busresponses.DataResponse;
import com.squareup.otto.Bus;

import org.json.JSONObject;

public abstract class BaseUseCase<T extends DataResponse> implements UseCase<T> {
    protected Bus bus;
    protected Bundle params;
    protected boolean forceRefresh = false;

    public BaseUseCase(Bus bus) {
        this.bus = bus;
    }

    protected boolean isValidResponse(DataResponse<JSONObject> res) {
        return res.getError() == 0;
    }

    /**
     * Get the params for this UseCase
     *
     * @return the params of this use case as a Bundle object, or null if no params are set
     */
    public Bundle getParams() {
        return params;
    }

    public void setParams(Bundle params) {
        this.params = params;
        this.forceRefresh = params.getBoolean(BasePresenter.FORCE_UPDATE, false);
    }

    /**
     * Post this DataResponse's response or error to the EventBus. Do not override this method, for more logic you should override
     * {@link #postResponseToBus(com.badoo.rateconversion.model.busresponses.DataResponse)} or {@link #postErrorToBus(com.badoo.rateconversion.model.busresponses.DataResponse)}
     *
     * @param response the response to be handled
     */
    protected final void handleResponse(T response) {
        if (hasError(response)) {
            postErrorToBus(response);
        } else {
            postResponseToBus(response);
        }
    }

    /**
     * Posts the DataResponse's error to the EventBus
     * Should override this method if you want to do more logic apart from posting the error
     *
     * @param response whose error will be posted to the EventBus
     */
    public void postErrorToBus(T response) {
        bus.post(Integer.valueOf(response.getError()));
    }

    /**
     * Checks if this DataResponse has a network or internal error
     *
     * @return true if {@link com.badoo.rateconversion.model.busresponses.DataResponse#getError()} is distinct than 0, false otherwise
     */
    public boolean hasError(T response) {
        int error = response.getError();
        return error != 0;
    }

    /**
     * Posts the DataResponse's response to the EventBus
     * Should override this method if you want to do more logic apart from posting the response
     *
     * @param response to be posted to the event bus
     */
    public void postResponseToBus(T response) {
        bus.post(response);
    }

}
