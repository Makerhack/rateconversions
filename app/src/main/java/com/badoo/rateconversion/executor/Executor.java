package com.badoo.rateconversion.executor;

import android.os.Bundle;

interface Executor {

    void execute(UseCase useCase, Bundle params);

}
