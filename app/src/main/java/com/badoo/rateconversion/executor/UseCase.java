package com.badoo.rateconversion.executor;

import android.os.Bundle;

import com.badoo.rateconversion.model.busresponses.DataResponse;


interface UseCase<T extends DataResponse> {

    void execute();

    void postErrorToBus(T response);

    boolean hasError(T response);

    void postResponseToBus(T response);

    void setParams(Bundle params);
}
